# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  length_hash = Hash.new
  str.split.each {|word| length_hash[word] = word.length}
  length_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash_arr = hash.to_a.sort_by {|key, value| value}
  hash_arr[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each {|key, value| older[key] = value}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_counts = Hash.new(0)
  word.each_char {|letter| letter_counts[letter] += 1}
  letter_counts
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  appearance_count = Hash.new(0)
  arr.each {|item| appearance_count[item] += 1}
  appearance_count.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_counts = Hash.new(0)
  numbers.each do |num|
    if num % 2 == 0
      even_odd_counts[:even] += 1
    else
      even_odd_counts[:odd] += 1
    end
  end
  even_odd_counts
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = ["a", "e", "i", "o", "u"]
  vowel_counts = Hash.new(0)
  string.chars.sort.reverse.each {|letter| vowel_counts[letter] += 1 if vowels.include?(letter)}
  vowel_counts.sort_by {|key, value| value}[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  late_month_students = students.select {|student, month| month > 6}
  late_names = late_month_students.map {|student, month| student}
  late_names.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  number_of_species = specimens.uniq.count
  population_counts = Hash.new(0)
  specimens.each {|animal| population_counts[animal] += 1}
  population_arr = population_counts.sort_by {|species, count| count}
  smallest_population_size = population_arr[0][1]
  largest_population_size = population_arr[-1][1]
  number_of_species**2 * smallest_population_size/largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_counts = character_count(normal_sign)
  character_count(vandalized_sign).each {|ltr, count| normal_counts[ltr] -= count}
  normal_counts.values.sort[0] >= 0
end

def character_count(str)
  counts_hash = Hash.new(0)
  str.downcase.gsub(" ", "").each_char {|ltr| counts_hash[ltr] += 1}
  counts_hash
end
